﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.Model
{
    public class CardioTypeDto
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
