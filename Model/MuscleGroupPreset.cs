﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.Model
{
    public class MuscleGroupPreset
    {
        public int id { get; set; }
        public string name { get; set; }
        public int musclegroupid { get; set; }
        public int presetid { get; set; }
        //public List<MuscleGroupDto> MuscleGroups = new List<MuscleGroupDto>();
    }
}
