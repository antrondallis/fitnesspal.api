﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.Model
{
    public class WorkoutForUpdate
    {
        public int id { get; set; }
        public int accountId { get; set; }
        public DateTime workoutDate { get; set; }
        public int MuscleGroupPresetId { get; set; }
        public int CardioTypeId { get; set; }
        public Boolean abs { get; set; }
    }
}
