﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.Model
{
    public class WeightDto
    {
        public int id { get; set; }
        public int accountId { get; set; }
        public double weight { get; set; }
        public DateTime weighInDate { get; set; }
    }
}
