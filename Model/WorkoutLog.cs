﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.Model
{
    public class WorkoutLog
    {
        public List<WorkoutDto> workouts = new List<WorkoutDto>();
    }
}
