﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.Model
{
    public class WorkoutDto
    {
        public int id { get; set; }
        public int accountId { get; set; }
        public DateTime workoutDate { get; set; }
        public int muscleGroupPresetId { get; set; }
        public string muscleGroupPresetName { get; set; }
        public int cardioId { get; set; }
        public string cardioName { get; set; }
        public string abs { get; set; }
    }
}
