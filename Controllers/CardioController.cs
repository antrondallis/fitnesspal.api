﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessPal.API.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FitnessPal.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class CardioController : Controller
    {
        private readonly ICardioRepository _cardioRepository;

        public CardioController(ICardioRepository cardioRepository)
        {
            _cardioRepository = cardioRepository;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _cardioRepository.GetAll();
            return Ok(result);
        }
        
        [HttpGet("{id}")]
        public IActionResult Index(int id)
        {
            var result = _cardioRepository.GetById(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}
