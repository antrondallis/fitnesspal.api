﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessPal.API.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FitnessPal.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class MuscleController : Controller
    {
        private readonly IMuscleGroupRepository _muscleGroupRepository;

        public MuscleController(IMuscleGroupRepository muscleGroupRepository)
        {
            _muscleGroupRepository = muscleGroupRepository;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var returnList = _muscleGroupRepository.GetAll();
            return Ok(returnList);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var returnMuscleGroup = _muscleGroupRepository.GetById(id);
            if (returnMuscleGroup == null)
                return NotFound();
            return Ok(returnMuscleGroup);
        }
    }
}