﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessPal.API.BusinessLogic;
using FitnessPal.API.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FitnessPal.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class WeightController : Controller
    {
        private readonly IWeightRepository _weightRepository;

        public WeightController(IWeightRepository weightRepository)
        {
            _weightRepository = weightRepository;
        }

        [HttpGet("{accountId}")]
        public IActionResult GetAll(int accountId)
        {
            var result = _weightRepository.GetAll(accountId);
            return Ok(result);
        }

        [HttpGet("{accountId}/{weightLogId}")]
        public IActionResult GetById(int accountId, int weightLogId)
        {
            var result = _weightRepository.GetById(accountId, weightLogId);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] WeightDto weight)
        {
            if (weight == null)
                return BadRequest();


            var result = _weightRepository.Add(weight);

            return Ok(result);
        }

        [HttpPut/*("{accountId}/{weightLogId}")*/]
        public IActionResult Update([FromBody] WeightDto weight)
        {
            if (weight == null)
                return BadRequest();

            var result = _weightRepository.Update(weight);
            return Ok(result);
        }

        [HttpDelete(/*"{accountId}/{workoutId}"*/)]
        public IActionResult Delete([FromBody] WeightDto weight/*int accountId, int workoutId*/)
        {
            var result = _weightRepository.Delete(weight);

            //_weightRepository.Delete(weightForDeletion);
            return Ok(result);
        }
    }
}