﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessPal.API.BusinessLogic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FitnessPal.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class MuscleGroupPresetController : Controller
    {
        private readonly IMuscleGroupPresetRepository _muscleGroupPresetRepository;

        public MuscleGroupPresetController(IMuscleGroupPresetRepository muscleGroupPresetRepository)
        {
            _muscleGroupPresetRepository = muscleGroupPresetRepository;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _muscleGroupPresetRepository.GetAll();
            return Ok(result);
        }
        
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var result = _muscleGroupPresetRepository.GetById(id);
            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}