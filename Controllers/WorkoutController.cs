﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitnessPal.API.BusinessLogic;
using FitnessPal.API.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FitnessPal.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class WorkoutController : Controller
    {
        private readonly IWorkoutRepository _workoutRepository;
        private readonly IMuscleGroupPresetRepository _mgpRepository;
        private readonly ICardioRepository _cardioRepository;

        public WorkoutController(IWorkoutRepository workoutRepository, IMuscleGroupPresetRepository mgpRepository, ICardioRepository cardioRepository)
        {
            _workoutRepository = workoutRepository;
            _mgpRepository = mgpRepository;
            _cardioRepository = cardioRepository;
        }

        [HttpGet("{accountId}")]
        public IActionResult GetAll(int accountId)
        {
            var result = _workoutRepository.GetAll(accountId);
            return Ok(result);
        }

        [HttpGet("getbymusclegroup/{accountId}/{muscleGroupId}")]
        public IActionResult GetByMuscleGroup(int accountId, int muscleGroupId)
        {
            var result = _workoutRepository.GetByMuscleGroup(accountId, muscleGroupId);
            return Ok(result);
        }

        [HttpGet("getallbydate/{accountId}")]
        public IActionResult GetAllByDate(int accountId)
        {
            var result = _workoutRepository.GetAllByDate(accountId);
            return Ok(result);
        }

        [HttpGet("{accountId}/{workoutId}")]
        public IActionResult GetById(int accountId, int workoutId)
        {
            var result = _workoutRepository.GetById(accountId, workoutId);
            return Ok(result);
        }        

        [HttpPost(Name = "CreateWorkout")]
        public IActionResult Create([FromBody] WorkoutDto workout)
        {
            var result = _workoutRepository.Add(workout);

            return Ok(result);
        }

        [HttpPut(Name = "UpdateWorkout")]
        public IActionResult Update([FromBody] WorkoutDto workout)
        {
            if (workout == null)
                return BadRequest();

            var result = _workoutRepository.Update(workout);

            return Ok(result);
        }

        [HttpDelete(Name = "DeleteWorkout")]
        public IActionResult Delete([FromBody] WorkoutDto workout)
        {
            var result = _workoutRepository.Delete(workout);
            return Ok(result);
        }
    }
}
