﻿using FitnessPal.API.BusinessLogic;
using FitnessPal.API.Model;
using FitnessPal.API.Model.UserModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace FitnessPal.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class AccountController: Controller
    {
        private readonly IUserModelRepository _userModelRepository;
        public AccountController(IUserModelRepository userModelRepository)
        {
            _userModelRepository = userModelRepository;
        }

        [HttpPost("RegisterAccount")]
        public IActionResult RegisterAccount([FromBody] UserAccountModel model)
        {
            var userNameResult = _userModelRepository.UsernameExist(model.Username);
            if (userNameResult.Data)
                return Ok(userNameResult);
            var newAccount = _userModelRepository.FormatAccountForCreation(model);
            var result = _userModelRepository.RegisterUserAccount(newAccount);

            return Ok(result);
        }

        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody]UserAccountModel model)
        {
            if (model.Username == null || model.PasswordHash == null)
                return BadRequest();

            var authAccount = _userModelRepository.FormatAccountForAuthentication(model);

            var result = _userModelRepository.Authenticate(authAccount);

            if (result != null)
            {
                result.StatusCode = (int)HttpStatusCode.OK;//200
                return Ok(result);
            }


            return NotFound();
        }

        //will add these later
        /*
        [HttpGet("{accountId}")]
        public IActionResult GetAccountInfoById(int accountId)
        {
            if (accountId == 0)
                return NoContent();
            var account = _accountRepository.GetById(accountId);

            if (account == null)
                return BadRequest();
            return Ok(account);
        }

        

        [HttpDelete("{accountId}/{currentAccountId}")]
        public IActionResult Delete(int accountId, int currentAccountId)
        {
            if (accountId != currentAccountId)
                return Unauthorized();

            _accountRepository.DeleteAccount(accountId);

            return NoContent();
        }
        */
    }
}
