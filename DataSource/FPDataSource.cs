﻿using FitnessPal.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.DataSource
{
    public class FPDataSource
    {
        public static FPDataSource Current { get; } = new FPDataSource();
        
        public AccountModel account { get; set; }
        public AccountModel account2 { get; set; }
        public List<MuscleGroupDto> MuscleGroupsSource { get; set; }
        public List<CardioTypeDto> cardioTypes { get; set; }

        public MuscleGroupPreset mgp1 { get; set; }
        public MuscleGroupPreset mgp2 { get; set; }
        public MuscleGroupPreset mgp3 { get; set; }
        public MuscleGroupPreset mgp4 { get; set; }

        public List<MuscleGroupPreset> mgpList { get; set; }

        public WorkoutLog workoutLog { get; set; }

        public List<WeightDto> weightLog = new List<WeightDto>();
        public List<AccountModel> accounts = new List<AccountModel>();

        public FPDataSource()
        {
            workoutLog = new WorkoutLog();
            Build();

        }

        public void Build()
        {
            account = new AccountModel()
            {
                accountId = 1,
                username = "testuser",
                email = "testuser@gmail.com",
                firstName = "Test",
                lastName = "User"
            };

            account2 = new AccountModel()
            {
                accountId = 2,
                username = "tesetuser2",
                email = "testuser2@gmail.com",
                firstName = "Test",
                lastName = "User2"
            };

            accounts.Add(account);
            accounts.Add(account2);

            MuscleGroupsSource = new List<MuscleGroupDto>()
            {
                new MuscleGroupDto { id = 1, name = "Chest"},
                new MuscleGroupDto { id = 2, name = "Triceps"},
                new MuscleGroupDto { id = 3, name = "Back"},
                new MuscleGroupDto { id = 4, name = "Biceps"},
                new MuscleGroupDto { id = 5, name = "Legs"},
                new MuscleGroupDto { id = 6, name = "Shoulders"}
            };

            cardioTypes = new List<CardioTypeDto>()
            {
                new CardioTypeDto(){ id = 1, name = "Treadmill" },
                new CardioTypeDto(){ id = 2, name = "Bike" },
                new CardioTypeDto(){ id = 3, name = "Eliptical" },
                new CardioTypeDto(){ id = 4, name = "Stair Master" }
            };

            //mgp1 = new MuscleGroupPreset() { id = 1, name = "Chest & Triceps" };
            //mgp1.MuscleGroups.Add(MuscleGroupsSource.First(g => g.id == 1));
            //mgp1.MuscleGroups.Add(MuscleGroupsSource.First(g => g.id == 2));

            //mgp2 = new MuscleGroupPreset() { id = 2, name = "Back & Biceps" };
            //mgp2.MuscleGroups.Add(MuscleGroupsSource.First(g => g.id == 3));
            //mgp2.MuscleGroups.Add(MuscleGroupsSource.First(g => g.id == 4));

            //mgp3 = new MuscleGroupPreset() { id = 3, name = "Legs" };
            //mgp3.MuscleGroups.Add(MuscleGroupsSource.First(g => g.id == 5));

            //mgp4 = new MuscleGroupPreset() { id = 4, name = "Shoulders" };
            //mgp4.MuscleGroups.Add(MuscleGroupsSource.First(g => g.id == 6));
        }

    }
}
