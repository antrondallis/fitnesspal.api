﻿using FitnessPal.API.DataSource;
using FitnessPal.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface IMuscleGroupRepository
    {
        ApiResult<List<MuscleGroupDto>> GetAll();
        ApiResult<MuscleGroupDto> GetById(int groupId);
    }
}
