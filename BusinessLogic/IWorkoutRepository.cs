﻿using FitnessPal.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface IWorkoutRepository
    {
        ApiResult<List<WorkoutDto>> GetAll(int accountId);
        ApiResult<List<WorkoutDto>> GetByMuscleGroup(int accountId, int muscleGroupId);
        ApiResult<WorkoutDto> GetById(int accountId, int workoutId);
        ApiResult<WorkoutDto> Add(WorkoutDto workout);
        ApiResult<WorkoutDto> Update(WorkoutDto workout);
        ApiResult<WorkoutDto> Delete(WorkoutDto workout);
        ApiResult<List<WorkoutDto>> GetAllByDate(int accountId);
    }
}
