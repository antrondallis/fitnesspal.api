﻿using FitnessPal.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface ICardioRepository
    {
        ApiResult<List<CardioTypeDto>> GetAll();
        ApiResult<CardioTypeDto> GetById(int cardioId);
    }
}
