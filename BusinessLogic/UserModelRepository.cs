﻿using FitnessPal.API.Model;
using FitnessPal.API.Model.UserModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TronProjectHelpers;

namespace FitnessPal.API.BusinessLogic
{
    public class UserModelRepository : IUserModelRepository
    {
        private readonly AppDbContext _appDbContext;
        private IConfiguration _config;

        public UserModelRepository(AppDbContext appDbContext, IConfiguration config)
        {
            _appDbContext = appDbContext;
            _config = config;
        }

        public UserAccountModel FormatAccountForCreation(UserAccountModel model)
        {
            UserAccountModel returnAccount = new UserAccountModel()
            {
                Username = model.Username,
                PasswordHash = SaltHash.Hash(model.PasswordHash),
                Email = model.Email,
                Firstname = model.Firstname,
                Lastname = model.Lastname
            };

            return returnAccount;
        }

        public UserAccountModel FormatAccountForAuthentication(UserAccountModel model)
        {
            model.PasswordHash = SaltHash.Hash(model.PasswordHash);

            return model;
        }

        public ApiResult<bool> UsernameExist(string username)
        {
            ApiResult<bool> result = new ApiResult<bool>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select Id " +
                    "from UserModel where username = @username collate sql_latin1_general_cp1_cs_as ";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data = true;
                            result.Message = "Username already Exists";
                            result.StatusCode = (int)HttpStatusCode.OK;//200
                        }
                        
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                    }
                }
            }

            return result;
        }

        public ApiResult<UserAccountModel> RegisterUserAccount(UserAccountModel model)
        {
            ApiResult<UserAccountModel> result = new ApiResult<UserAccountModel>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "insert into UserModel (Username, PasswordHash, Email, Firstname, Lastname) " +
                    "values(@username, @passwordhash, @email, @firstname, @lastname)";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = model.Username;
                    cmd.Parameters.Add("@passwordhash", SqlDbType.VarChar).Value = model.PasswordHash;
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = model.Email;
                    cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = model.Firstname;
                    cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = model.Lastname;


                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result.Message = "SUCESS";
                        result.StatusCode = (int)HttpStatusCode.Created;//201
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            return result;
        }

        public ApiResult<UserAccountModel> Authenticate(UserAccountModel model)
        {
            ApiResult<UserAccountModel> result = new ApiResult<UserAccountModel>();
            UserAccountModel returnAccount = new UserAccountModel();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select Id, Username, Email, Firstname, Lastname " +
                    "from UserModel where username = @username collate sql_latin1_general_cp1_cs_as " +
                    "and PasswordHash = @passwordhash collate sql_latin1_general_cp1_cs_as";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = model.Username;
                    cmd.Parameters.Add("@passwordhash", SqlDbType.VarChar).Value = model.PasswordHash;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            returnAccount.Id = Convert.ToInt32(reader[0].ToString());
                            returnAccount.Username = reader[1].ToString().Trim();
                            returnAccount.Email = reader[2].ToString().Trim();
                            returnAccount.Firstname = reader[3].ToString().Trim();
                            returnAccount.Lastname = reader[4].ToString().Trim();
                        }
                        result.Data = returnAccount;
                        result.Message = "SUCCESS";
                        result.StatusCode = (int)HttpStatusCode.OK;//200
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                    }
                }
            }

            return result;
        }
    }
}
