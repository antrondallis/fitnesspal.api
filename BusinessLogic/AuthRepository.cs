﻿using FitnessPal.API.AuthClasses;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public class AuthRepository : IAuthRepository
    {
        private IConfiguration _config;

        public AuthRepository(IConfiguration config)
        {
            _config = config;
        }

        public UserModel Authenticate(LoginModel login)
        {
            UserModel user = null;

            if(login.Username == "f!tN3s$" && login.Password == "63S60a9")
            {
                user = new UserModel { Name = "Fitness Pal", Email = "fitnesspal@domain.com" };
            }

            return user;
        }

        public string BuildToken(UserModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(60),
              signingCredentials: creds);

            var test = _config["Jwt:Key"];
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
