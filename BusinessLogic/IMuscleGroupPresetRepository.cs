﻿using FitnessPal.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface IMuscleGroupPresetRepository
    {
        ApiResult<List<MuscleGroupPreset>> GetAll();
        ApiResult<MuscleGroupPreset> GetById(int id);
    }
}
