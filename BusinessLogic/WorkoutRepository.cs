﻿using FitnessPal.API.DataSource;
using FitnessPal.API.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public class WorkoutRepository: IWorkoutRepository
    {
        #region Constructor
        private IConfiguration _config;

        public WorkoutRepository(IConfiguration config)
        {
            _config = config;
        }
        #endregion

        #region GetAll
        public ApiResult<List<WorkoutDto>> GetAll(int accountId)
        {
            ApiResult<List<WorkoutDto>> result = new ApiResult<List<WorkoutDto>>()
            {
                Data = new List<WorkoutDto>()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = @"select distinct workoutlog.id, workoutdate, MuscleGroupPresetId, musclegrouppreset.name,
                                CardioId, ct.name, case abs when 'Y' then 'Yes' else 'No' end as 'Abs'
                                from workoutlog
                                inner join musclegrouppreset on presetid = musclegrouppresetid
                                inner join CardioType ct on ct.Id = CardioId
                                where accountId = @accountId";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new WorkoutDto
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                workoutDate = Convert.ToDateTime(reader[1].ToString().Trim()),
                                muscleGroupPresetId = Convert.ToInt32(reader[2].ToString()),
                                muscleGroupPresetName = reader[3].ToString().Trim(),
                                cardioId = Convert.ToInt32(reader[4].ToString()),
                                cardioName = reader[5].ToString().Trim(),
                                abs = reader[6].ToString()
                            });
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;

                    }
                }
            }

            return result;
        }
        #endregion

        #region GetAllByDate
        public ApiResult<List<WorkoutDto>> GetAllByDate(int accountId)
        {
            ApiResult<List<WorkoutDto>> result = new ApiResult<List<WorkoutDto>>()
            {
                Data = new List<WorkoutDto>()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = @"select distinct workoutlog.id, 
                            workoutdate workoutdate, MuscleGroupPresetId, musclegrouppreset.name,
                            CardioId, ct.name, case abs when 'Y' then 'Yes' else 'No' end as 'Abs'
                            from workoutlog
                            inner join musclegrouppreset on presetid = musclegrouppresetid
                            inner join CardioType ct on ct.Id = CardioId
                            where accountId = @accountId
                            order by WorkoutDate desc";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountId", System.Data.SqlDbType.Int).Value = accountId;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new WorkoutDto
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                workoutDate = Convert.ToDateTime(reader[1].ToString().Trim()),
                                muscleGroupPresetId = Convert.ToInt32(reader[2].ToString()),
                                muscleGroupPresetName = reader[3].ToString().Trim(),
                                cardioId = Convert.ToInt32(reader[4].ToString()),
                                cardioName = reader[5].ToString().Trim(),
                                abs = reader[6].ToString()
                            });
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;

                    }
                }
            }

            return result;
        }
        #endregion

        #region GetByMuscleGroup
        public ApiResult<List<WorkoutDto>> GetByMuscleGroup(int accountId, int muscleGroupId)
        {
            ApiResult<List<WorkoutDto>> result = new ApiResult<List<WorkoutDto>>()
            {
                Data = new List<WorkoutDto>()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = @"select distinct workoutlog.id, workoutdate, MuscleGroupPresetId, musclegrouppreset.name,
                                CardioId, ct.name, case abs when 'Y' then 'True' else 'False' end as 'Abs'
                                from workoutlog
                                inner join musclegrouppreset on presetid = musclegrouppresetid
                                inner join CardioType ct on ct.Id = CardioId
                                where accountId = @accountid
                                and MuscleGroupId = @musclegroupid";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = accountId;
                    cmd.Parameters.Add("@musclegroupid", System.Data.SqlDbType.Int).Value = muscleGroupId;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new WorkoutDto
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                workoutDate = Convert.ToDateTime(reader[1].ToString().Trim()),
                                muscleGroupPresetId = Convert.ToInt32(reader[2].ToString()),
                                muscleGroupPresetName = reader[3].ToString().Trim(),
                                cardioId = Convert.ToInt32(reader[4].ToString()),
                                cardioName = reader[5].ToString().Trim(),
                                abs = reader[6].ToString()
                            });
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
        #endregion

        #region GetById
        public ApiResult<WorkoutDto> GetById(int accountId, int workoutId)
        {
            ApiResult<WorkoutDto> result = new ApiResult<WorkoutDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = @"select distinct workoutlog.id, workoutdate, MuscleGroupPresetId, musclegrouppreset.name,
                                CardioId, ct.name, case abs when 'Y' then 'True' else 'False' end as 'Abs'
                                from workoutlog
                                inner join musclegrouppreset on presetid = musclegrouppresetid
                                inner join CardioType ct on ct.Id = CardioId
                                where accountId = @accountid
                                and workoutlog.id = @workoutid";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = accountId;
                    cmd.Parameters.Add("@workoutid", System.Data.SqlDbType.Int).Value = workoutId;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data = new WorkoutDto()
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                workoutDate = Convert.ToDateTime(reader[1].ToString().Trim()),
                                muscleGroupPresetId = Convert.ToInt32(reader[2].ToString()),
                                muscleGroupPresetName = reader[3].ToString().Trim(),
                                cardioId = Convert.ToInt32(reader[4].ToString()),
                                cardioName = reader[5].ToString().Trim(),
                                abs = reader[6].ToString()
                            };
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
        #endregion

        #region Add
        public ApiResult<WorkoutDto> Add(WorkoutDto workout)
        {
            ApiResult<WorkoutDto> result = new ApiResult<WorkoutDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = @"insert into workoutlog (accountid, MuscleGroupPresetId, CardioId, WorkoutDate, abs)
                                values (@accountid, @musclegrouppresetid, @cardioid, @workoutdate, @abs)";
                    

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = workout.accountId;
                    cmd.Parameters.Add("@musclegrouppresetid", System.Data.SqlDbType.Int).Value = workout.muscleGroupPresetId;
                    cmd.Parameters.Add("@cardioid", System.Data.SqlDbType.Int).Value = workout.cardioId;
                    cmd.Parameters.Add("@workoutdate", System.Data.SqlDbType.Date).Value = workout.workoutDate;
                    cmd.Parameters.Add("@abs", System.Data.SqlDbType.VarChar).Value = workout.abs;

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result.StatusCode = (int)HttpStatusCode.Created;
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
        #endregion

        #region Update
        public ApiResult<WorkoutDto> Update(WorkoutDto workout)
        {
            ApiResult<WorkoutDto> result = new ApiResult<WorkoutDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = @"update workoutlog
                                set MuscleGroupPresetId = @mgpid,
                                cardioid = @cardioid,
                                workoutdate = @workoutdate,
                                abs = @abs
                                where id = @id
                                and accountId = @accountid";


                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = workout.accountId;
                    cmd.Parameters.Add("@mgpid", System.Data.SqlDbType.Int).Value = workout.muscleGroupPresetId;
                    cmd.Parameters.Add("@cardioid", System.Data.SqlDbType.Int).Value = workout.cardioId;
                    cmd.Parameters.Add("@workoutdate", System.Data.SqlDbType.Date).Value = workout.workoutDate;
                    cmd.Parameters.Add("@abs", System.Data.SqlDbType.VarChar).Value = workout.abs;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = workout.id;

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result.StatusCode = (int)HttpStatusCode.OK;
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
        #endregion

        #region Delete
        public ApiResult<WorkoutDto> Delete(WorkoutDto workout)
        {
            ApiResult<WorkoutDto> result = new ApiResult<WorkoutDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "delete from workoutlog where id = @id and accountid = @accountid";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = workout.id;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = workout.accountId;

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result.Message = "SUCCESS";
                        result.StatusCode = (int)HttpStatusCode.NoContent;
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
