﻿using FitnessPal.API.AuthClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface IAuthRepository
    {
        UserModel Authenticate(LoginModel login);
        string BuildToken(UserModel user);
    }
}
