﻿using FitnessPal.API.DataSource;
using FitnessPal.API.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public class MuscleGroupPresetRepository: IMuscleGroupPresetRepository
    {
        private IConfiguration _config;

        public MuscleGroupPresetRepository(IConfiguration config)
        {
            _config = config;
        }

        public ApiResult<List<MuscleGroupPreset>> GetAll()
        {
            ApiResult<List<MuscleGroupPreset>> result = new ApiResult<List<MuscleGroupPreset>>()
            {
                Data = new List<MuscleGroupPreset>()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select distinct presetid, name from musclegrouppreset;";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new MuscleGroupPreset
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                name = reader[1].ToString().Trim()
                            });
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;

                    }
                }
            }

            return result;
        }

        public ApiResult<MuscleGroupPreset> GetById(int id)
        {
            ApiResult<MuscleGroupPreset> result = new ApiResult<MuscleGroupPreset>()
            {
                Data = new MuscleGroupPreset()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select distinct presetid, name from musclegrouppreset where presetid = @id";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.id = Convert.ToInt32(reader[0].ToString());
                            result.Data.name = reader[1].ToString();
                            result.StatusCode = (int)HttpStatusCode.OK;
                            result.Message = "SUCCESS";
                        }
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
    }
}
