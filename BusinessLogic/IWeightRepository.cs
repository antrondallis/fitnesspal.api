﻿using FitnessPal.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface IWeightRepository
    {
        ApiResult<List<WeightDto>> GetAll(int accountId);
        ApiResult<WeightDto> GetById(int accountId, int weightLogId);
        ApiResult<WeightDto> Add(WeightDto weight);
        ApiResult<WeightDto> Update(WeightDto weight);
        ApiResult<WeightDto> Delete(WeightDto weight);
    }
}
