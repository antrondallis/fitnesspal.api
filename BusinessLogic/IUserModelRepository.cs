﻿using FitnessPal.API.AuthClasses;
using FitnessPal.API.Model;
using FitnessPal.API.Model.UserModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public interface IUserModelRepository
    {
        UserAccountModel FormatAccountForCreation(UserAccountModel model);
        UserAccountModel FormatAccountForAuthentication(UserAccountModel model);
        ApiResult<bool> UsernameExist(string username);
        ApiResult<UserAccountModel> RegisterUserAccount(UserAccountModel model);
        ApiResult<UserAccountModel> Authenticate(UserAccountModel model);

    }
}
