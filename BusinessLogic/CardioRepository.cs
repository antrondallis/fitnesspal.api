﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FitnessPal.API.DataSource;
using FitnessPal.API.Model;
using Microsoft.Extensions.Configuration;

namespace FitnessPal.API.BusinessLogic
{
    public class CardioRepository: ICardioRepository
    {
        private IConfiguration _config;

        public CardioRepository(IConfiguration config)
        {
            _config = config;
        }
        public ApiResult<List<CardioTypeDto>> GetAll()
        {
            ApiResult<List<CardioTypeDto>> result = new ApiResult<List<CardioTypeDto>>()
            {
                Data = new List<CardioTypeDto>()
            };
            

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select id, name from cardiotype order by id";

                using(SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new CardioTypeDto
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                name = reader[1].ToString().Trim()
                            });
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;

                    }
                }
            }

            return result;
        }

        public ApiResult<CardioTypeDto> GetById(int cardioId)
        {
            ApiResult<CardioTypeDto> result = new ApiResult<CardioTypeDto>()
            {
                Data = new CardioTypeDto()
            };
            
            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select * from cardiotype where id = @id";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = cardioId;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.id = Convert.ToInt32(reader[0].ToString());
                            result.Data.name = reader[1].ToString();
                            result.StatusCode = (int)HttpStatusCode.OK;
                            result.Message = "SUCCESS";
                        }
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
    }
}
