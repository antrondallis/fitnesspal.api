﻿using FitnessPal.API.DataSource;
using FitnessPal.API.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public class MuscleGroupRepository: IMuscleGroupRepository
    {
        private IConfiguration _config;

        public MuscleGroupRepository(IConfiguration config)
        {
            _config = config;
        }

        public ApiResult<List<MuscleGroupDto>> GetAll()
        {
            ApiResult<List<MuscleGroupDto>> result = new ApiResult<List<MuscleGroupDto>>()
            {
                Data = new List<MuscleGroupDto>()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select id, name from musclegroup";

                using(SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new MuscleGroupDto
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                name = reader[1].ToString().Trim()
                            });
                            result.StatusCode = (int)HttpStatusCode.OK;//200
                            result.Message = "SUCCESS";
                        }
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }
            return result;
        }

        public ApiResult<MuscleGroupDto> GetById(int groupId)
        {
            ApiResult<MuscleGroupDto> result = new ApiResult<MuscleGroupDto>()
            {
                Data = new MuscleGroupDto()
            };

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select * from musclegroup where id = @id";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = groupId;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.id = Convert.ToInt32(reader[0].ToString());
                            result.Data.name = reader[1].ToString();
                            result.StatusCode = (int)HttpStatusCode.OK;
                            result.Message = "SUCCESS";
                        }
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
    }
}
