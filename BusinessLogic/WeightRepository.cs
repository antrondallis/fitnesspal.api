﻿using FitnessPal.API.DataSource;
using FitnessPal.API.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FitnessPal.API.BusinessLogic
{
    public class WeightRepository : IWeightRepository
    {
        #region constructor
        private IConfiguration _config;
        public WeightRepository(IConfiguration config)
        {
            _config = config;
        }
        #endregion

        #region GetAll
        public ApiResult<List<WeightDto>> GetAll(int accountId)
        {
            ApiResult<List<WeightDto>> result = new ApiResult<List<WeightDto>>()
            {
                Data = new List<WeightDto>()
            };


            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select * from weightlog where accountid = @accountid order by weighInDate desc";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = accountId;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data.Add(new WeightDto
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                accountId = Convert.ToInt32(reader[1].ToString().Trim()),
                                weight = Convert.ToDouble(reader[2].ToString()),
                                weighInDate = Convert.ToDateTime(reader[3].ToString())
                            });
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;

                    }
                }
            }

            return result;
        }
        #endregion

        #region GetById
        public ApiResult<WeightDto> GetById(int accountId, int weightLogId)
        {
            ApiResult<WeightDto> result = new ApiResult<WeightDto>();


            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "select * from weightlog where accountid = @accountid and id = @weightlogid";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = accountId;
                    cmd.Parameters.Add("@weightlogid", System.Data.SqlDbType.Int).Value = weightLogId;
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            result.Data = new WeightDto()
                            {
                                id = Convert.ToInt32(reader[0].ToString()),
                                accountId = Convert.ToInt32(reader[1].ToString().Trim()),
                                weight = Convert.ToDouble(reader[2].ToString()),
                                weighInDate = Convert.ToDateTime(reader[3].ToString())
                            };
                        }

                        result.StatusCode = (int)HttpStatusCode.OK;//200
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"Error Occured: {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;

                    }
                }
            }

            return result;
        }
        #endregion

        #region Add
        public ApiResult<WeightDto> Add(WeightDto weight)
        {
            ApiResult<WeightDto> result = new ApiResult<WeightDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "insert into weightlog (accountid, weight, weighindate) " +
                    "values(@accountid, @weight, @weighindate)";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = weight.accountId;
                    cmd.Parameters.Add("@weight", System.Data.SqlDbType.Decimal).Value = weight.weight;
                    cmd.Parameters.Add("@weighindate", System.Data.SqlDbType.Date).Value = weight.weighInDate;

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result.StatusCode = (int)HttpStatusCode.Created;
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
        #endregion

        #region Update
        public ApiResult<WeightDto> Update(WeightDto weight)
        {
            ApiResult<WeightDto> result = new ApiResult<WeightDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "update weightlog set weight = @weight,  weighindate = @date where accountid = @accountid and id = @id";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@weight", System.Data.SqlDbType.Decimal).Value = weight.weight;
                    cmd.Parameters.Add("@date", System.Data.SqlDbType.Date).Value = weight.weighInDate;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = weight.accountId;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = weight.id;

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        result.Data = new WeightDto()
                        {
                            id = weight.id,
                            accountId = weight.accountId,
                            weight = weight.weight,
                            weighInDate = weight.weighInDate
                        };
                        result.StatusCode = (int)HttpStatusCode.OK;
                        result.Message = "SUCCESS";
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }

            return result;
        }
        #endregion

        #region Delete
        public ApiResult<WeightDto> Delete(WeightDto weight)
        {
            ApiResult<WeightDto> result = new ApiResult<WeightDto>();

            using (SqlConnection conn = new SqlConnection(_config["ConnectionStrings:DefaultConnection"]))
            {
                string sql = "delete from weightlog where id = @id and accountid = @accountid";

                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = weight.id;
                    cmd.Parameters.Add("@accountid", System.Data.SqlDbType.Int).Value = weight.accountId;

                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        result.Message = "SUCCESS";
                        result.StatusCode = (int)HttpStatusCode.NoContent;
                    }
                    catch (Exception e)
                    {
                        result.Message = $"FAIL {e.Message}";
                        result.StatusCode = (int)HttpStatusCode.BadRequest;
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
